################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Peripherals/wind/wind.c 

OBJS += \
./Peripherals/wind/wind.o 

C_DEPS += \
./Peripherals/wind/wind.d 


# Each subdirectory must supply rules for building sources it contributes
Peripherals/wind/%.o Peripherals/wind/%.su: ../Peripherals/wind/%.c Peripherals/wind/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I"D:/SmallDesign/CitizenScience/Weather_Station/Mausam_PCB_V1.2_STM32/Peripherals" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Peripherals-2f-wind

clean-Peripherals-2f-wind:
	-$(RM) ./Peripherals/wind/wind.d ./Peripherals/wind/wind.o ./Peripherals/wind/wind.su

.PHONY: clean-Peripherals-2f-wind

