/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2023 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "i2c.h"
#include "rtc.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include<wind/wind.h>
#include<stdio.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

// Refer to examples : https://controllerstech.com/input-capture-in-stm32/
#define TIM2CLOCK   72000000 // 72 MHz
#define PRESCALAR  1		// results in 72MHz
/*
 * TIM2_APB_clock_freq/Prescalar/ARR = freq. of final clock
 * 1/40kHz = 2.5e-5 seconds period
 * this period can be sampled at 72MHz max, where each pulse is 1/72MHz seconds wide
 * therefore
 * (1/40kHz)/(1/72MHz) = 1800 counts
 * where each count amounts to 1/72MHz s = 1.38e-8 s or 13.8 nano seconds.
 */


uint32_t timer_1s=0;
uint32_t timer_50ms=0;


//void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim){
//
//	switch(htim->Channel){
//	case HAL_TIM_ACTIVE_CHANNEL_1:
//		if((sampling_flag==1) && (channel_check_flag_D_N == 0)){
//			D_N_sample = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1);
//			channel_check_flag_D_N=1;
//		}
//		break;
//
//	case HAL_TIM_ACTIVE_CHANNEL_2:
//		if((sampling_flag==1) && (channel_check_flag_D_SE == 0)){
//			D_SE_sample = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_2);
//			channel_check_flag_D_SE=1;
//		}
//		break;
//
//	case HAL_TIM_ACTIVE_CHANNEL_3:
//		if((sampling_flag==1) && (channel_check_flag_D_SW == 0)){
//			D_SW_sample = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_3);
//			channel_check_flag_D_SW = 1;
//		}
//		break;
//	}
//}

// ADC call back function
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	adc_val = HAL_ADC_GetValue(&hadc1);
	/*If continuousconversion mode is DISABLED uncomment below*/
	//HAL_ADC_Start_IT (&hadc1);
}

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void)
{
	/* USER CODE BEGIN 1 */

	uint8_t MSG[500] = {'\0'};

	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_TIM2_Init();
	MX_USART1_UART_Init();
	MX_TIM1_Init();
	MX_I2C1_Init();
	MX_RTC_Init();
	MX_SPI1_Init();
	MX_USART3_UART_Init();
	MX_TIM3_Init();
	MX_ADC1_Init();
	/* USER CODE BEGIN 2 */

	// Start the PWM signals on Tim1 (as well as its complementary)
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
	HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_1);

	// Initiate interrupts on 3 channels of TIM2
	//HAL_TIM_IC_Start_IT(&htim3,TIM_CHANNEL_1);
	//	HAL_TIM_IC_Start_IT(&htim2,TIM_CHANNEL_2);
	//	HAL_TIM_IC_Start_IT(&htim2,TIM_CHANNEL_3);

	static unsigned short pin_state = 0;

	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1)
	{
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */

		//		if(HAL_GetTick()-timer_50ms > 50){
		//			if(WS_sample()==1)
		//				timer_50ms = HAL_GetTick();
		//		}

		WS_sample();

		if(HAL_GetTick()-timer_1s > 2000){
			timer_1s = HAL_GetTick();
			pin_state = !pin_state;

			// LED toggle
			HAL_GPIO_WritePin(LED_OUT_GPIO_Port, LED_OUT_Pin, pin_state);

			//WS_compute();

			sprintf(MSG, "ADC_N=%f, ADC_SE=%f, ADC_SW=%f \n \\
						 	A=%lf, B=%lf, C=%f , U[0]=%lf, U[1]=%lf, K=%lf\n \\
							a=%f, b=%f\n \\
							Speed0=%f, Dir0=%f, Speed1=%f, Dir1=%f \n",
							WG_value[0],WG_value[1],WG_value[2],
							A,B,C, U[0],U[1], K,
							a,b,
							wind_speed[0], wind_direction_deg[0],wind_speed[1], wind_direction_deg[1]);

			HAL_UART_Transmit(&huart1, MSG, sizeof(MSG), 1000);
		}

		/* USER CODE END 3 */
	}
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void)
{
	RCC_OscInitTypeDef RCC_OscInitStruct = {0};
	RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
	RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

	/** Initializes the RCC Oscillators according to the specified parameters
	 * in the RCC_OscInitTypeDef structure.
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.LSIState = RCC_LSI_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		Error_Handler();
	}

	/** Initializes the CPU, AHB and APB buses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
			|RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
	{
		Error_Handler();
	}
	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_ADC;
	PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
	PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
	{
		Error_Handler();
	}
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void)
{
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	__disable_irq();
	while (1)
	{
	}
	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t *file, uint32_t line)
{
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
