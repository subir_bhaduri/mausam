/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define WG_US_Rx_Tx_Phase_Diff_Amp_V_Pin GPIO_PIN_0
#define WG_US_Rx_Tx_Phase_Diff_Amp_V_GPIO_Port GPIOA
#define MEM_WP_Pin GPIO_PIN_3
#define MEM_WP_GPIO_Port GPIOA
#define SPI_CS1_Pin GPIO_PIN_4
#define SPI_CS1_GPIO_Port GPIOA
#define LED_OUT_Pin GPIO_PIN_0
#define LED_OUT_GPIO_Port GPIOB
#define SIM800_Sleep_Pin GPIO_PIN_1
#define SIM800_Sleep_GPIO_Port GPIOB
#define SIM800_TX3_Pin GPIO_PIN_10
#define SIM800_TX3_GPIO_Port GPIOB
#define SIM800_RX3_Pin GPIO_PIN_11
#define SIM800_RX3_GPIO_Port GPIOB
#define WG_US_Tx_SqWN_Pin GPIO_PIN_13
#define WG_US_Tx_SqWN_GPIO_Port GPIOB
#define WG_Enable_Pin GPIO_PIN_14
#define WG_Enable_GPIO_Port GPIOB
#define WG_US_Tx_SqW_Pin GPIO_PIN_8
#define WG_US_Tx_SqW_GPIO_Port GPIOA
#define TX1_Pin GPIO_PIN_9
#define TX1_GPIO_Port GPIOA
#define RG_Enable_Pin GPIO_PIN_3
#define RG_Enable_GPIO_Port GPIOB
#define RG_Freq_SqW_Pin GPIO_PIN_4
#define RG_Freq_SqW_GPIO_Port GPIOB
#define MUX_S0_Pin GPIO_PIN_8
#define MUX_S0_GPIO_Port GPIOB
#define MUX_S1_Pin GPIO_PIN_9
#define MUX_S1_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
