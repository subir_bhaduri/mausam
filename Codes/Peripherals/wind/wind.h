
#ifndef wind_H
#define wind_H

#include <math.h>
#include <stdint.h>
#include <tim.h>

#define D_array_size 100
#define R 0.03	// in meters
#define smoothing_factor 0.15

extern double a, b, K, A, B, C;
extern uint16_t D_N_sample, D_SE_sample, D_SW_sample;
//extern uint16_t D_N[D_array_size], D_SE[D_array_size], D_SW[D_array_size];
extern uint8_t idx;
extern float D_N_avg, D_SE_avg, D_SW_avg;
extern float wind_speed[2], wind_direction_deg[2];
extern double U[2];

extern uint8_t channel_check_flag_D_N;
extern uint8_t channel_check_flag_D_SE;
extern uint8_t	channel_check_flag_D_SW;

extern uint8_t sampling_flag;

uint8_t WS_sample();
void WS_compute();

#endif
