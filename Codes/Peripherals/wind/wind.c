
#include<wind/wind.h>


double a0,b0, a, b, K, A, B, C;
uint16_t D_N_sample, D_SE_sample, D_SW_sample;
uint16_t D_N[D_array_size], D_SE[D_array_size], D_SW[D_array_size];
uint8_t idx;
float D_N_avg, D_SE_avg, D_SW_avg;
float wind_speed[2], wind_direction_deg[2];
double U[2];

uint8_t channel_check_flag_D_N = 0;
uint8_t channel_check_flag_D_SE = 0;
uint8_t	channel_check_flag_D_SW = 0;

uint8_t sampling_flag=0;


uint32_t WG_sampling_period_ms = 500;
uint32_t WG_timer;

uint16_t adc_val=0;
float WG_value[3];
uint8_t MUX_count=0;

enum MUX_params{
	MUX_ch1=0,
	MUX_ch2,
	MUX_ch3
};

uint8_t WS_init(){
	// Start the WS channel
	HAL_GPIO_WritePin(WG_Enable_GPIO_Port, WG_Enable_Pin, 1);
	// Start the ADC in interrupt mode
	HAL_ADC_Start_IT (&hadc1);
}

uint8_t WS_stop(){
	HAL_ADC_Stop(&hadc1); // stop adc
}

uint8_t WS_sample(){
	/* Round robbin
	 * 00 North
	 * 01 South east
	 * 10 South West
	 * 11 xx
	 *
	 * Switch channels after a time gap
	 */

	if(HAL_GetTick() - WG_timer >= WG_sampling_period_ms){

		MUX_count++;
		if(MUX_count > MUX_ch3) MUX_count = 0;

		switch(MUX_count){
		case MUX_ch1:
			HAL_GPIO_WritePin(MUX_S0_GPIO_Port, MUX_S0_Pin, 0);
			HAL_GPIO_WritePin(MUX_S1_GPIO_Port, MUX_S1_Pin, 0);
			break;

		case MUX_ch2:
			HAL_GPIO_WritePin(MUX_S0_GPIO_Port, MUX_S0_Pin, 1);
			HAL_GPIO_WritePin(MUX_S1_GPIO_Port, MUX_S1_Pin, 0);
			break;

		case MUX_ch3:
			HAL_GPIO_WritePin(MUX_S0_GPIO_Port, MUX_S0_Pin, 0);
			HAL_GPIO_WritePin(MUX_S1_GPIO_Port, MUX_S1_Pin, 1);
			break;
		}

		WG_value[MUX_count] =  adc_val/4096.0*3.3;
		WG_timer = HAL_GetTick();
	}
	return 0;
}


void WS_compute(){
	// First compute K from given info
	A = ( 3.0 * pow(pow(D_SW_avg,2) - pow(D_SE_avg,2) , 2) + pow( pow(D_SW_avg,2) + pow(D_SE_avg,2) - 2.0 * pow(D_N_avg,2) , 2 )  ) / ( 36.0 * pow(R,2));
	B = - (pow(D_SW_avg,2) + pow(D_SE_avg,2) + pow(D_N_avg,2))/3.0;
	C = pow(R,2);

	U[0] = (- B - pow(pow(B,2) - 4*A*C,0.5))/(2.0*A);
	U[1] = (- B + pow(pow(B,2) - 4*A*C,0.5))/(2.0*A);

	// The compute the X(b) and Y(a) intercepts of the wind vector
	a0 = (pow(D_SW_avg,2) - pow(D_SE_avg,2)) / (3.464*R);
	b0 = (pow(D_SW_avg,2) + pow(D_SE_avg,2) - 2* pow(D_N_avg,2))/(6.0*R);


	if(U[0] > 0){
		K = pow(U[0],0.5);

		a = pow(K,2) * a0;
		b = pow(K,2) * b0;

		// Finally, compute the speed and direction.
		wind_speed[0] = pow(pow(a,2) + pow(b,2),0.5);
		wind_direction_deg[0] = atan(b/a)*180.0/3.14;
	}
	else{
		// Finally, compute the speed and direction.
		wind_speed[0] = 0;
		wind_direction_deg[0] = 0;
	}

	if(U[1] > 0){
		K = pow(U[1],0.5);

		a = pow(K,2) * a0;
		b = pow(K,2) * b0;

		// Finally, compute the speed and direction.
		wind_speed[1] = pow(pow(a,2) + pow(b,2),0.5);
		wind_direction_deg[1] = atan(b/a)*180.0/3.14;
	}

	else{
		// Finally, compute the speed and direction.
		wind_speed[1] =0;
		wind_direction_deg[1] = 0;
	}

}
