EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R?
U 1 1 692DB510
P 7650 3675
AR Path="/692DB510" Ref="R?"  Part="1" 
AR Path="/692C545E/692DB510" Ref="R?"  Part="1" 
F 0 "R?" V 7857 3675 50  0000 C CNN
F 1 "4.7k" V 7766 3675 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 7580 3675 50  0001 C CNN
F 3 "~" H 7650 3675 50  0001 C CNN
	1    7650 3675
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 4075 7650 4075
$Comp
L Connector:TestPoint TR?
U 1 1 692DB517
P 8125 3075
AR Path="/692DB517" Ref="TR?"  Part="1" 
AR Path="/692C545E/692DB517" Ref="TR?"  Part="1" 
F 0 "TR?" H 8175 3225 50  0000 L CNN
F 1 "TestPoint" H 8000 3300 50  0000 L CNN
F 2 "TestPoint:TestPoint_THTPad_1.0x1.0mm_Drill0.5mm" H 8325 3075 50  0001 C CNN
F 3 "~" H 8325 3075 50  0001 C CNN
	1    8125 3075
	-1   0    0    -1  
$EndComp
Wire Notes Line
	9300 3575 9100 3575
Wire Notes Line
	9300 3525 9100 3525
Wire Notes Line
	9250 3650 9100 3650
Wire Notes Line
	9225 3675 9150 3675
Wire Notes Line
	9200 3700 9175 3700
Wire Notes Line
	9200 3575 9200 3650
Wire Notes Line
	9200 3375 9200 3525
Wire Wire Line
	8725 3550 8825 3550
Wire Wire Line
	8825 3350 8825 3550
Wire Wire Line
	8125 4075 8125 3550
Wire Wire Line
	8125 3550 8350 3550
Wire Wire Line
	8350 3550 8425 3550
Connection ~ 8350 3550
Wire Wire Line
	8350 3650 8350 3550
$Comp
L Device:R R?
U 1 1 692DB52F
P 8350 3800
AR Path="/692DB52F" Ref="R?"  Part="1" 
AR Path="/692C545E/692DB52F" Ref="R?"  Part="1" 
F 0 "R?" V 8450 3800 50  0000 C CNN
F 1 "1.5k" V 8250 3800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 8280 3800 50  0001 C CNN
F 3 "~" H 8350 3800 50  0001 C CNN
	1    8350 3800
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 692DB535
P 8575 3550
AR Path="/692DB535" Ref="R?"  Part="1" 
AR Path="/692C545E/692DB535" Ref="R?"  Part="1" 
F 0 "R?" V 8675 3500 50  0000 C CNN
F 1 "10k" V 8475 3525 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 8505 3550 50  0001 C CNN
F 3 "~" H 8575 3550 50  0001 C CNN
	1    8575 3550
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TR?
U 1 1 692DB53B
P 3875 3075
AR Path="/692DB53B" Ref="TR?"  Part="1" 
AR Path="/692C545E/692DB53B" Ref="TR?"  Part="1" 
F 0 "TR?" H 3925 3225 50  0000 L CNN
F 1 "TestPoint" H 3750 3300 50  0000 L CNN
F 2 "TestPoint:TestPoint_THTPad_1.0x1.0mm_Drill0.5mm" H 4075 3075 50  0001 C CNN
F 3 "~" H 4075 3075 50  0001 C CNN
	1    3875 3075
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TR?
U 1 1 692DB541
P 5400 3150
AR Path="/692DB541" Ref="TR?"  Part="1" 
AR Path="/692C545E/692DB541" Ref="TR?"  Part="1" 
F 0 "TR?" H 5450 3300 50  0000 L CNN
F 1 "TestPoint" H 5275 3375 50  0000 L CNN
F 2 "TestPoint:TestPoint_THTPad_1.0x1.0mm_Drill0.5mm" H 5600 3150 50  0001 C CNN
F 3 "~" H 5600 3150 50  0001 C CNN
	1    5400 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 4275 7800 5025
Wire Wire Line
	7550 4275 7800 4275
$Comp
L power:GND #PWR?
U 1 1 692DB549
P 7800 5025
AR Path="/692DB549" Ref="#PWR?"  Part="1" 
AR Path="/692C545E/692DB549" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7800 4775 50  0001 C CNN
F 1 "GND" H 7805 4852 50  0000 C CNN
F 2 "" H 7800 5025 50  0001 C CNN
F 3 "" H 7800 5025 50  0001 C CNN
	1    7800 5025
	1    0    0    -1  
$EndComp
Connection ~ 3050 3525
Wire Notes Line
	2575 3550 2775 3550
Wire Notes Line
	2575 3500 2775 3500
Wire Notes Line
	2625 3625 2775 3625
Wire Notes Line
	2650 3650 2725 3650
Wire Notes Line
	2675 3675 2700 3675
Wire Notes Line
	2675 3550 2675 3625
Wire Notes Line
	2675 3350 2675 3500
Wire Wire Line
	3150 3525 3050 3525
Wire Wire Line
	3050 3325 3050 3525
Wire Notes Line
	4225 3575 4425 3575
Wire Notes Line
	4225 3525 4425 3525
Wire Notes Line
	4275 3650 4425 3650
Wire Notes Line
	4300 3675 4375 3675
Wire Notes Line
	4325 3700 4350 3700
Wire Notes Line
	4325 3575 4325 3650
Wire Notes Line
	4325 3375 4325 3525
Wire Wire Line
	4700 3550 4700 4575
Connection ~ 4700 3550
Wire Wire Line
	4800 3550 4700 3550
Wire Wire Line
	4700 3350 4700 3550
Connection ~ 5400 3550
Wire Wire Line
	5175 3950 5175 4675
Wire Wire Line
	5400 4075 5400 3550
Connection ~ 3525 4475
Wire Wire Line
	3525 3925 3525 4475
Wire Wire Line
	3875 3525 3875 4175
Wire Wire Line
	3525 3525 3450 3525
Connection ~ 3525 3525
Wire Wire Line
	3525 3625 3525 3525
$Comp
L Device:R R?
U 1 1 692DB57A
P 3525 3775
AR Path="/692DB57A" Ref="R?"  Part="1" 
AR Path="/692C545E/692DB57A" Ref="R?"  Part="1" 
F 0 "R?" V 3625 3775 50  0000 C CNN
F 1 "1.5k" V 3425 3775 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 3455 3775 50  0001 C CNN
F 3 "~" H 3525 3775 50  0001 C CNN
	1    3525 3775
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 692DB580
P 3300 3525
AR Path="/692DB580" Ref="R?"  Part="1" 
AR Path="/692C545E/692DB580" Ref="R?"  Part="1" 
F 0 "R?" V 3400 3475 50  0000 C CNN
F 1 "10k" V 3200 3500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 3230 3525 50  0001 C CNN
F 3 "~" H 3300 3525 50  0001 C CNN
	1    3300 3525
	0    1    1    0   
$EndComp
Wire Wire Line
	5400 3550 5175 3550
Wire Wire Line
	5175 3550 5100 3550
Connection ~ 5175 3550
Wire Wire Line
	5175 3650 5175 3550
Wire Wire Line
	2750 4625 2750 4750
Connection ~ 2750 4625
Wire Wire Line
	3525 4625 3525 4475
Wire Wire Line
	2525 5050 2750 5050
Connection ~ 2525 5050
Wire Wire Line
	2300 5050 2525 5050
Wire Wire Line
	2750 4575 2750 4625
Wire Wire Line
	2300 4675 2300 4750
$Comp
L Device:R R?
U 1 1 692DB592
P 2750 4900
AR Path="/692DB592" Ref="R?"  Part="1" 
AR Path="/692C545E/692DB592" Ref="R?"  Part="1" 
F 0 "R?" H 2875 4925 50  0000 C CNN
F 1 "1.5k" H 2900 4825 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 2680 4900 50  0001 C CNN
F 3 "~" H 2750 4900 50  0001 C CNN
	1    2750 4900
	1    0    0    -1  
$EndComp
Connection ~ 2750 4275
$Comp
L Device:R R?
U 1 1 692DB599
P 2750 4425
AR Path="/692DB599" Ref="R?"  Part="1" 
AR Path="/692C545E/692DB599" Ref="R?"  Part="1" 
F 0 "R?" H 2875 4450 50  0000 C CNN
F 1 "1.5k" H 2900 4350 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 2680 4425 50  0001 C CNN
F 3 "~" H 2750 4425 50  0001 C CNN
	1    2750 4425
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 692DB59F
P 5175 3800
AR Path="/692DB59F" Ref="R?"  Part="1" 
AR Path="/692C545E/692DB59F" Ref="R?"  Part="1" 
F 0 "R?" V 5275 3800 50  0000 C CNN
F 1 "1.5k" V 5075 3800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 5105 3800 50  0001 C CNN
F 3 "~" H 5175 3800 50  0001 C CNN
	1    5175 3800
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 692DB5A5
P 4950 3550
AR Path="/692DB5A5" Ref="R?"  Part="1" 
AR Path="/692C545E/692DB5A5" Ref="R?"  Part="1" 
F 0 "R?" V 5050 3500 50  0000 C CNN
F 1 "10k" V 4850 3525 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 4880 3550 50  0001 C CNN
F 3 "~" H 4950 3550 50  0001 C CNN
	1    4950 3550
	0    1    1    0   
$EndComp
Connection ~ 2300 4675
Wire Wire Line
	2300 4575 2300 4675
$Comp
L power:GND #PWR?
U 1 1 692DB5AD
P 2525 5050
AR Path="/692DB5AD" Ref="#PWR?"  Part="1" 
AR Path="/692C545E/692DB5AD" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2525 4800 50  0001 C CNN
F 1 "GND" H 2530 4877 50  0000 C CNN
F 2 "" H 2525 5050 50  0001 C CNN
F 3 "" H 2525 5050 50  0001 C CNN
	1    2525 5050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 692DB5B3
P 2300 4900
AR Path="/692DB5B3" Ref="R?"  Part="1" 
AR Path="/692C545E/692DB5B3" Ref="R?"  Part="1" 
F 0 "R?" H 2425 4925 50  0000 C CNN
F 1 "1.5k" H 2450 4825 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 2230 4900 50  0001 C CNN
F 3 "~" H 2300 4900 50  0001 C CNN
	1    2300 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 4275 2750 4275
$Comp
L Device:R R?
U 1 1 692DB5BB
P 2300 4425
AR Path="/692DB5BB" Ref="R?"  Part="1" 
AR Path="/692C545E/692DB5BB" Ref="R?"  Part="1" 
F 0 "R?" H 2425 4450 50  0000 C CNN
F 1 "1.5k" H 2450 4350 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 2230 4425 50  0001 C CNN
F 3 "~" H 2300 4425 50  0001 C CNN
	1    2300 4425
	1    0    0    -1  
$EndComp
Wire Wire Line
	1675 4100 1675 4275
Wire Wire Line
	1675 4275 1675 4325
Connection ~ 1675 4275
$Comp
L power:GND #PWR?
U 1 1 692DB5C6
P 1675 4625
AR Path="/692DB5C6" Ref="#PWR?"  Part="1" 
AR Path="/692C545E/692DB5C6" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1675 4375 50  0001 C CNN
F 1 "GND" H 1680 4452 50  0000 C CNN
F 2 "" H 1675 4625 50  0001 C CNN
F 3 "" H 1675 4625 50  0001 C CNN
	1    1675 4625
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 692DB5CC
P 1675 4475
AR Path="/692DB5CC" Ref="C?"  Part="1" 
AR Path="/692C545E/692DB5CC" Ref="C?"  Part="1" 
F 0 "C?" V 1950 4525 50  0000 C CNN
F 1 "0.1uF" V 1850 4525 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 1713 4325 50  0001 C CNN
F 3 "~" H 1675 4475 50  0001 C CNN
	1    1675 4475
	-1   0    0    1   
$EndComp
$Comp
L SamacSys_Parts:LM339DR IC?
U 1 1 692DB5D8
P 6450 4075
AR Path="/692DB5D8" Ref="IC?"  Part="1" 
AR Path="/692C545E/692DB5D8" Ref="IC?"  Part="1" 
F 0 "IC?" H 7000 4340 50  0000 C CNN
F 1 "LM339DR" H 7000 4249 50  0000 C CNN
F 2 "SamacSys_Parts:SOIC127P600X175-14N" H 7400 4175 50  0001 L CNN
F 3 "http://www.ti.com/lit/gpn/lm339" H 7400 4075 50  0001 L CNN
F 4 "Quad Differential Comparator" H 7400 3975 50  0001 L CNN "Description"
F 5 "1.75" H 7400 3875 50  0001 L CNN "Height"
F 6 "595-LM339DR" H 7400 3775 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Texas-Instruments/LM339DR?qs=vxEfx8VrU7Aps9Hd52ppTA%3D%3D" H 7400 3675 50  0001 L CNN "Mouser Price/Stock"
F 8 "Texas Instruments" H 7400 3575 50  0001 L CNN "Manufacturer_Name"
F 9 "LM339DR" H 7400 3475 50  0001 L CNN "Manufacturer_Part_Number"
	1    6450 4075
	1    0    0    -1  
$EndComp
Text Notes 5350 5100 0    50   ~ 0
Capacitive Rain Measurement Circuit \nref: https://romanblack.com/onesec/CapMeter.htm
$Comp
L Device:R R?
U 1 1 692DB5E0
P 6250 3675
AR Path="/692DB5E0" Ref="R?"  Part="1" 
AR Path="/692C545E/692DB5E0" Ref="R?"  Part="1" 
F 0 "R?" V 6457 3675 50  0000 C CNN
F 1 "4.7k" V 6366 3675 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 6180 3675 50  0001 C CNN
F 3 "~" H 6250 3675 50  0001 C CNN
	1    6250 3675
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 692DB5E6
P 5875 3675
AR Path="/692DB5E6" Ref="R?"  Part="1" 
AR Path="/692C545E/692DB5E6" Ref="R?"  Part="1" 
F 0 "R?" V 6082 3675 50  0000 C CNN
F 1 "4.7k" V 5991 3675 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 5805 3675 50  0001 C CNN
F 3 "~" H 5875 3675 50  0001 C CNN
	1    5875 3675
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 4075 6250 4075
Wire Wire Line
	3525 4475 6450 4475
Wire Wire Line
	4700 4575 6450 4575
Wire Wire Line
	5175 4675 6450 4675
Wire Wire Line
	6250 4075 6250 3825
Connection ~ 6250 4075
Wire Wire Line
	6250 4075 6450 4075
Wire Wire Line
	5875 4175 5875 3825
Connection ~ 5875 4175
Wire Wire Line
	5875 4175 6450 4175
Wire Wire Line
	7650 4075 7650 3825
Connection ~ 7650 4075
Wire Wire Line
	7650 4075 8125 4075
Wire Wire Line
	5875 3525 6250 3525
Wire Wire Line
	6250 3525 6925 3525
Connection ~ 6250 3525
Wire Wire Line
	6925 3325 6925 3525
Connection ~ 6925 3525
Wire Wire Line
	6925 3525 7650 3525
Wire Wire Line
	7550 4575 8350 4575
Wire Wire Line
	8350 3950 8350 4575
Wire Wire Line
	8825 3550 8825 4675
Wire Wire Line
	8825 4675 7550 4675
Connection ~ 8825 3550
NoConn ~ 7550 4175
NoConn ~ 7550 4375
NoConn ~ 7550 4475
Text Label 5475 6350 2    50   ~ 0
GND
Text Label 5475 6450 2    50   ~ 0
RG_RefC_Raw
$Comp
L Connector:Conn_01x04_Female J?
U 1 1 692E5D63
P 5675 6450
AR Path="/692E5D63" Ref="J?"  Part="1" 
AR Path="/692C545E/692E5D63" Ref="J?"  Part="1" 
F 0 "J?" H 5703 6426 50  0000 L CNN
F 1 "Conn_01x04_Female" H 5703 6335 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 5675 6450 50  0001 C CNN
F 3 "~" H 5675 6450 50  0001 C CNN
	1    5675 6450
	1    0    0    -1  
$EndComp
Text Notes 5025 6100 0    50   ~ 0
Rain gauge capacitors
$Comp
L Device:R R?
U 1 1 693214F0
P 5175 1975
AR Path="/693214F0" Ref="R?"  Part="1" 
AR Path="/69125121/693214F0" Ref="R?"  Part="1" 
AR Path="/692C545E/693214F0" Ref="R?"  Part="1" 
F 0 "R?" V 5275 1925 50  0000 C CNN
F 1 "1k" V 5075 1950 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 5105 1975 50  0001 C CNN
F 3 "~" H 5175 1975 50  0001 C CNN
	1    5175 1975
	0    -1   1    0   
$EndComp
Wire Wire Line
	4900 1975 5025 1975
Wire Wire Line
	5700 1600 5700 1775
$Comp
L 2N2222A:2N2222A_SOT23 Q?
U 1 1 693214FD
P 5600 1975
AR Path="/693214FD" Ref="Q?"  Part="1" 
AR Path="/69125121/693214FD" Ref="Q?"  Part="1" 
AR Path="/692C545E/693214FD" Ref="Q?"  Part="1" 
F 0 "Q?" H 5799 2021 50  0000 L CNN
F 1 "2N2222A_SOT23" H 5799 1930 50  0000 L CNN
F 2 "TO92254P470H750-3" H 5600 1975 50  0001 L BNN
F 3 "" H 5600 1975 50  0001 L BNN
F 4 "N/A" H 5600 1975 50  0001 L BNN "PARTREV"
F 5 "7.5 mm" H 5600 1975 50  0001 L BNN "MAXIMUM_PACKAGE_HEIGHT"
F 6 "121774" H 5600 1975 50  0001 L BNN "SNAPEDA_PACKAGE_ID"
F 7 "IPC 7351B" H 5600 1975 50  0001 L BNN "STANDARD"
F 8 "Diotec Semiconductor" H 5600 1975 50  0001 L BNN "MANUFACTURER"
	1    5600 1975
	1    0    0    -1  
$EndComp
Wire Wire Line
	5325 1975 5500 1975
Text Label 5825 2375 0    50   ~ 0
RG_Power_3.3V
Wire Wire Line
	5700 2175 5700 2375
Wire Wire Line
	5700 2375 5825 2375
Text GLabel 5525 1600 0    50   Input ~ 0
3.3V
Wire Wire Line
	5525 1600 5700 1600
Text HLabel 4900 1975 0    50   Input ~ 0
RG_control
Text Label 6925 3325 2    50   ~ 0
RG_Power_3.3V
Text Label 1675 4100 2    50   ~ 0
RG_Power_3.3V
Text Label 5475 6550 2    50   ~ 0
RG_EnvC_Raw
Text Label 5475 6650 2    50   ~ 0
RG_MainC_Raw
Text Label 3050 3325 2    50   ~ 0
RG_RefC_Raw
Text HLabel 3050 3075 0    50   Output ~ 0
RG_RefC_SqW
Connection ~ 3875 3525
Wire Wire Line
	3875 4175 5875 4175
Wire Wire Line
	3525 3525 3875 3525
Wire Wire Line
	2750 4625 3525 4625
Wire Wire Line
	3050 3525 3050 4375
Wire Wire Line
	3050 4375 6450 4375
Wire Wire Line
	2300 4675 5175 4675
Connection ~ 5175 4675
Wire Wire Line
	2750 4275 6450 4275
Wire Wire Line
	1675 4275 2300 4275
Connection ~ 2300 4275
Text Label 4700 3350 2    50   ~ 0
RG_EnvC_Raw
Text HLabel 4700 3150 0    50   Output ~ 0
RG_EnvC_SqW
Wire Wire Line
	3875 3075 3050 3075
Wire Wire Line
	3875 3075 3875 3525
Connection ~ 3875 3075
Wire Wire Line
	4700 3150 5400 3150
Wire Wire Line
	5400 3150 5400 3550
Connection ~ 5400 3150
Wire Wire Line
	8125 3075 8825 3075
Wire Wire Line
	8125 3075 8125 3550
Connection ~ 8125 3075
Connection ~ 8125 3550
$Comp
L power:GND #PWR?
U 1 1 6935F766
P 9050 5075
AR Path="/6935F766" Ref="#PWR?"  Part="1" 
AR Path="/692C545E/6935F766" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9050 4825 50  0001 C CNN
F 1 "GND" H 9055 4902 50  0000 C CNN
F 2 "" H 9050 5075 50  0001 C CNN
F 3 "" H 9050 5075 50  0001 C CNN
	1    9050 5075
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 6935F76C
P 9050 4850
AR Path="/6935F76C" Ref="R?"  Part="1" 
AR Path="/692C545E/6935F76C" Ref="R?"  Part="1" 
F 0 "R?" H 9175 4875 50  0000 C CNN
F 1 "1.5k" H 9200 4775 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 8980 4850 50  0001 C CNN
F 3 "~" H 9050 4850 50  0001 C CNN
	1    9050 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 6935F772
P 9050 4375
AR Path="/6935F772" Ref="R?"  Part="1" 
AR Path="/692C545E/6935F772" Ref="R?"  Part="1" 
F 0 "R?" H 9175 4400 50  0000 C CNN
F 1 "1.5k" H 9200 4300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 8980 4375 50  0001 C CNN
F 3 "~" H 9050 4375 50  0001 C CNN
	1    9050 4375
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 4525 9050 4575
Wire Wire Line
	9050 5000 9050 5075
Wire Wire Line
	8350 4575 9050 4575
Connection ~ 8350 4575
Connection ~ 9050 4575
Wire Wire Line
	9050 4575 9050 4700
Text Label 9050 4150 0    50   ~ 0
RG_Power_3.3V
Wire Wire Line
	9050 4150 9050 4225
Text HLabel 8825 3075 2    50   Output ~ 0
RG_MainC_SqW
Text Label 8825 3350 0    50   ~ 0
RG_MainC_Raw
$EndSCHEMATC
