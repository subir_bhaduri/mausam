EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	3875 5200 3800 5200
$Comp
L Device:C C?
U 1 1 69186CEE
P 2700 4450
AR Path="/69186CEE" Ref="C?"  Part="1" 
AR Path="/6915B170/69186CEE" Ref="C?"  Part="1" 
F 0 "C?" V 2975 4500 50  0000 C CNN
F 1 "0.1uF" V 2875 4500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 2738 4300 50  0001 C CNN
F 3 "~" H 2700 4450 50  0001 C CNN
	1    2700 4450
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 69186CF4
P 2800 4775
AR Path="/69186CF4" Ref="#PWR?"  Part="1" 
AR Path="/6915B170/69186CF4" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2800 4525 50  0001 C CNN
F 1 "GND" H 2805 4602 50  0000 C CNN
F 2 "" H 2800 4775 50  0001 C CNN
F 3 "" H 2800 4775 50  0001 C CNN
	1    2800 4775
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 4250 2700 4300
$Comp
L Device:R R?
U 1 1 69186CFC
P 3550 4525
AR Path="/69186CFC" Ref="R?"  Part="1" 
AR Path="/6915B170/69186CFC" Ref="R?"  Part="1" 
F 0 "R?" V 3757 4525 50  0000 C CNN
F 1 "4.7k" V 3666 4525 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 3480 4525 50  0001 C CNN
F 3 "~" H 3550 4525 50  0001 C CNN
	1    3550 4525
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 69186D02
P 3250 4525
AR Path="/69186D02" Ref="R?"  Part="1" 
AR Path="/6915B170/69186D02" Ref="R?"  Part="1" 
F 0 "R?" V 3457 4525 50  0000 C CNN
F 1 "4.7k" V 3366 4525 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 3180 4525 50  0001 C CNN
F 3 "~" H 3250 4525 50  0001 C CNN
	1    3250 4525
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 69186D08
P 5125 4525
AR Path="/69186D08" Ref="R?"  Part="1" 
AR Path="/6915B170/69186D08" Ref="R?"  Part="1" 
F 0 "R?" V 5332 4525 50  0000 C CNN
F 1 "4.7k" V 5241 4525 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 5055 4525 50  0001 C CNN
F 3 "~" H 5125 4525 50  0001 C CNN
	1    5125 4525
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 4250 2950 4250
Wire Wire Line
	3250 4375 3250 4250
Wire Wire Line
	3550 4375 3550 4250
Wire Wire Line
	5125 4375 5125 4250
Wire Wire Line
	5125 5000 4975 5000
Connection ~ 5125 5000
Wire Wire Line
	5125 4675 5125 5000
Wire Wire Line
	3550 4675 3550 5000
Wire Wire Line
	3250 4675 3250 5100
Connection ~ 3250 5100
Connection ~ 3550 4250
Connection ~ 3550 5000
Wire Wire Line
	3550 5000 3875 5000
Wire Wire Line
	3250 4250 3550 4250
Wire Wire Line
	3550 4250 3800 4250
Wire Wire Line
	3250 5100 3875 5100
Wire Wire Line
	3800 5200 3800 4250
Connection ~ 3800 4250
$Comp
L power:GND #PWR?
U 1 1 69186D22
P 3725 6350
AR Path="/69186D22" Ref="#PWR?"  Part="1" 
AR Path="/6915B170/69186D22" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3725 6100 50  0001 C CNN
F 1 "GND" H 3730 6177 50  0000 C CNN
F 2 "" H 3725 6350 50  0001 C CNN
F 3 "" H 3725 6350 50  0001 C CNN
	1    3725 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3875 5500 3725 5500
Text Label 1925 5600 2    50   ~ 0
WG_US_Rx_N_Amp
Text Label 1925 5400 2    50   ~ 0
WG_US_Rx_SE_Amp
$Comp
L Device:C C?
U 1 1 69186D2B
P 2625 5600
AR Path="/69186D2B" Ref="C?"  Part="1" 
AR Path="/6915B170/69186D2B" Ref="C?"  Part="1" 
F 0 "C?" V 2700 5425 50  0000 C CNN
F 1 "0.1uF" V 2800 5425 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 2663 5450 50  0001 C CNN
F 3 "~" H 2625 5600 50  0001 C CNN
	1    2625 5600
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 69186D31
P 2625 5400
AR Path="/69186D31" Ref="C?"  Part="1" 
AR Path="/6915B170/69186D31" Ref="C?"  Part="1" 
F 0 "C?" V 2400 5575 50  0000 C CNN
F 1 "0.1uF" V 2500 5575 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 2663 5250 50  0001 C CNN
F 3 "~" H 2625 5400 50  0001 C CNN
	1    2625 5400
	0    1    1    0   
$EndComp
Wire Wire Line
	3400 5300 3875 5300
Wire Wire Line
	3400 6275 3725 6275
Connection ~ 3725 6275
Wire Wire Line
	4975 5600 5175 5600
Wire Wire Line
	3725 6275 5175 6275
$Comp
L Device:C C?
U 1 1 69186D3D
P 5750 5500
AR Path="/69186D3D" Ref="C?"  Part="1" 
AR Path="/6915B170/69186D3D" Ref="C?"  Part="1" 
F 0 "C?" V 5850 5625 50  0000 C CNN
F 1 "0.1uF" V 5950 5650 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 5788 5350 50  0001 C CNN
F 3 "~" H 5750 5500 50  0001 C CNN
	1    5750 5500
	0    1    1    0   
$EndComp
Text Label 6325 5500 0    50   ~ 0
WG_US_Rx_SW_Amp
Wire Wire Line
	4975 5200 6325 5200
Wire Wire Line
	3725 6275 3725 6350
$Comp
L SamacSys_Parts:LM339DR IC?
U 1 1 69186D50
P 3875 5000
AR Path="/69186D50" Ref="IC?"  Part="1" 
AR Path="/6915B170/69186D50" Ref="IC?"  Part="1" 
F 0 "IC?" H 4425 5265 50  0000 C CNN
F 1 "LM339DR" H 4425 5174 50  0000 C CNN
F 2 "SamacSys_Parts:SOIC127P600X175-14N" H 4825 5100 50  0001 L CNN
F 3 "http://www.ti.com/lit/gpn/lm339" H 4825 5000 50  0001 L CNN
F 4 "Quad Differential Comparator" H 4825 4900 50  0001 L CNN "Description"
F 5 "1.75" H 4825 4800 50  0001 L CNN "Height"
F 6 "595-LM339DR" H 4825 4700 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Texas-Instruments/LM339DR?qs=vxEfx8VrU7Aps9Hd52ppTA%3D%3D" H 4825 4600 50  0001 L CNN "Mouser Price/Stock"
F 8 "Texas Instruments" H 4825 4500 50  0001 L CNN "Manufacturer_Name"
F 9 "LM339DR" H 4825 4400 50  0001 L CNN "Manufacturer_Part_Number"
	1    3875 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 4250 5125 4250
NoConn ~ 4975 5300
NoConn ~ 4975 5400
NoConn ~ 4975 5100
Wire Wire Line
	3400 5300 3400 6275
Wire Wire Line
	3725 5500 3725 6275
Wire Wire Line
	5175 5600 5175 6275
Wire Wire Line
	1925 5600 2475 5600
Wire Wire Line
	1925 5400 2475 5400
Wire Wire Line
	5125 5000 5850 5000
Wire Wire Line
	5900 5500 6325 5500
Wire Wire Line
	2775 5600 3875 5600
Wire Wire Line
	2775 5400 3875 5400
Wire Wire Line
	4975 5500 5600 5500
$Comp
L Connector:TestPoint TN?
U 1 1 69186D64
P 2000 4550
AR Path="/69186D64" Ref="TN?"  Part="1" 
AR Path="/6915B170/69186D64" Ref="TN4"  Part="1" 
F 0 "TN4" H 1925 4775 50  0000 L CNN
F 1 "TestPoint" V 1875 4600 50  0000 L CNN
F 2 "TestPoint:TestPoint_THTPad_1.0x1.0mm_Drill0.5mm" H 2200 4550 50  0001 C CNN
F 3 "~" H 2200 4550 50  0001 C CNN
	1    2000 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 5100 2275 5100
$Comp
L Connector:TestPoint TE?
U 1 1 69186D6B
P 2275 4550
AR Path="/69186D6B" Ref="TE?"  Part="1" 
AR Path="/6915B170/69186D6B" Ref="TE4"  Part="1" 
F 0 "TE4" H 2200 4775 50  0000 L CNN
F 1 "TestPoint" V 2125 4625 50  0000 L CNN
F 2 "TestPoint:TestPoint_THTPad_1.0x1.0mm_Drill0.5mm" H 2475 4550 50  0001 C CNN
F 3 "~" H 2475 4550 50  0001 C CNN
	1    2275 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 5000 2000 5000
Wire Wire Line
	2000 4550 2000 5000
Connection ~ 2000 5000
Wire Wire Line
	2000 5000 3550 5000
Wire Wire Line
	2275 4550 2275 5100
$Comp
L Connector:TestPoint TW?
U 1 1 69186D76
P 5850 4500
AR Path="/69186D76" Ref="TW?"  Part="1" 
AR Path="/6915B170/69186D76" Ref="TW4"  Part="1" 
F 0 "TW4" H 5775 4725 50  0000 L CNN
F 1 "TestPoint" H 5675 4800 50  0000 L CNN
F 2 "TestPoint:TestPoint_THTPad_1.0x1.0mm_Drill0.5mm" H 6050 4500 50  0001 C CNN
F 3 "~" H 6050 4500 50  0001 C CNN
	1    5850 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 4500 5850 5000
Connection ~ 5850 5000
Wire Wire Line
	5850 5000 6325 5000
$Comp
L Device:C C?
U 1 1 69186D7F
P 2950 4450
AR Path="/69186D7F" Ref="C?"  Part="1" 
AR Path="/6915B170/69186D7F" Ref="C?"  Part="1" 
F 0 "C?" V 2875 4575 50  0000 C CNN
F 1 "1uF" V 2775 4600 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 2988 4300 50  0001 C CNN
F 3 "~" H 2950 4450 50  0001 C CNN
	1    2950 4450
	-1   0    0    1   
$EndComp
Connection ~ 2275 5100
Wire Wire Line
	2275 5100 3250 5100
Wire Wire Line
	2950 4300 2950 4250
Connection ~ 2950 4250
Wire Wire Line
	2950 4250 3250 4250
Connection ~ 3250 4250
Wire Wire Line
	2700 4600 2700 4775
Wire Wire Line
	2700 4775 2800 4775
Wire Wire Line
	2800 4775 2950 4775
Wire Wire Line
	2950 4775 2950 4600
Connection ~ 2800 4775
$Sheet
S 8725 3150 1200 700 
U 691AAF6E
F0 "US_amplifier_N.sch" 50
F1 "US_amplifier.sch" 50
F2 "raw_signal" I L 8725 3525 50 
F3 "Output" O R 9925 3525 50 
$EndSheet
$Sheet
S 8725 4175 1200 700 
U 691AAF72
F0 "US_amplifier_SE.sch" 50
F1 "US_amplifier.sch" 50
F2 "raw_signal" I L 8725 4550 50 
F3 "Output" O R 9925 4550 50 
$EndSheet
$Sheet
S 8700 5200 1200 700 
U 691AAF76
F0 "US_amplifier_SW.sch" 50
F1 "US_amplifier.sch" 50
F2 "raw_signal" I L 8700 5575 50 
F3 "Output" O R 9900 5575 50 
$EndSheet
Text Label 8325 3525 2    50   ~ 0
WG_US_Rx_N_Raw
Wire Wire Line
	8325 3525 8725 3525
Text Label 8325 4550 2    50   ~ 0
WG_US_Rx_SE_Raw
Wire Wire Line
	8325 4550 8725 4550
Text Label 8300 5575 2    50   ~ 0
WG_US_Rx_SW_Raw
Wire Wire Line
	8300 5575 8700 5575
Text Label 10300 3525 0    50   ~ 0
WG_US_Rx_N_Amp
Wire Wire Line
	10300 3525 9925 3525
Text Label 10300 4550 0    50   ~ 0
WG_US_Rx_SE_Amp
Wire Wire Line
	10300 4550 9925 4550
Text Label 10275 5575 0    50   ~ 0
WG_US_Rx_SW_Amp
Wire Wire Line
	10275 5575 9900 5575
$Comp
L Device:R R?
U 1 1 691CC084
P 5300 2000
AR Path="/691CC084" Ref="R?"  Part="1" 
AR Path="/6915B170/691CC084" Ref="R?"  Part="1" 
F 0 "R?" V 5450 2000 50  0000 C CNN
F 1 "100k" V 5375 2000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 5230 2000 50  0001 C CNN
F 3 "~" H 5300 2000 50  0001 C CNN
	1    5300 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 691CC08A
P 5300 2350
AR Path="/691CC08A" Ref="R?"  Part="1" 
AR Path="/6915B170/691CC08A" Ref="R?"  Part="1" 
F 0 "R?" V 5450 2350 50  0000 C CNN
F 1 "100k" V 5375 2350 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 5230 2350 50  0001 C CNN
F 3 "~" H 5300 2350 50  0001 C CNN
	1    5300 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 2150 5300 2175
$Comp
L Device:C C?
U 1 1 691CC092
P 5825 2350
AR Path="/691CC092" Ref="C?"  Part="1" 
AR Path="/6915B170/691CC092" Ref="C?"  Part="1" 
F 0 "C?" V 6100 2400 50  0000 C CNN
F 1 "0.1uF" V 6000 2400 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 5863 2200 50  0001 C CNN
F 3 "~" H 5825 2350 50  0001 C CNN
	1    5825 2350
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 691CC098
P 5550 2750
AR Path="/691CC098" Ref="#PWR?"  Part="1" 
AR Path="/6915B170/691CC098" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5550 2500 50  0001 C CNN
F 1 "GND" H 5555 2577 50  0000 C CNN
F 2 "" H 5550 2750 50  0001 C CNN
F 3 "" H 5550 2750 50  0001 C CNN
	1    5550 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 2175 5825 2175
Connection ~ 5300 2175
Wire Wire Line
	5300 2175 5300 2200
Wire Wire Line
	5825 2650 5550 2650
Wire Wire Line
	5300 2500 5300 2650
Wire Wire Line
	5300 2650 5550 2650
Connection ~ 5550 2650
Wire Wire Line
	5550 2650 5550 2750
Wire Wire Line
	5825 2175 5825 2200
Wire Wire Line
	5825 2500 5825 2650
Connection ~ 5825 2175
Wire Wire Line
	5825 1825 5825 2175
Text GLabel 5975 1825 2    50   Output ~ 0
Vref_1.65V
Wire Wire Line
	5825 1825 5975 1825
Text HLabel 1900 5000 0    50   Output ~ 0
WG_US_Rx_N_SqW
Text HLabel 1900 5100 0    50   Output ~ 0
WG_US_Rx_SE_SqW
Text HLabel 6325 5000 2    50   Output ~ 0
WG_US_Rx_SW_SqW
$Comp
L Connector:Conn_01x02_Female J?
U 1 1 691DD9F0
P 2650 2550
AR Path="/691DD9F0" Ref="J?"  Part="1" 
AR Path="/6915B170/691DD9F0" Ref="J?"  Part="1" 
F 0 "J?" H 2678 2526 50  0000 L CNN
F 1 "Conn_01x02_Female" H 2678 2435 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-02A_1x02_P2.54mm_Vertical" H 2650 2550 50  0001 C CNN
F 3 "~" H 2650 2550 50  0001 C CNN
	1    2650 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 2550 2450 2550
Text Label 2150 2650 2    50   ~ 0
GND
Wire Wire Line
	2150 2650 2450 2650
$Comp
L Connector:Conn_01x02_Female J?
U 1 1 691DD9F9
P 2650 2825
AR Path="/691DD9F9" Ref="J?"  Part="1" 
AR Path="/6915B170/691DD9F9" Ref="J?"  Part="1" 
F 0 "J?" H 2678 2801 50  0000 L CNN
F 1 "Conn_01x02_Female" H 2678 2710 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-02A_1x02_P2.54mm_Vertical" H 2650 2825 50  0001 C CNN
F 3 "~" H 2650 2825 50  0001 C CNN
	1    2650 2825
	1    0    0    -1  
$EndComp
Text Label 2150 2825 2    50   ~ 0
WG_US_Rx_SE_Raw
Wire Wire Line
	2150 2825 2450 2825
Text Label 2150 2925 2    50   ~ 0
GND
Wire Wire Line
	2150 2925 2450 2925
$Comp
L Connector:Conn_01x02_Female J?
U 1 1 691DDA03
P 2650 3175
AR Path="/691DDA03" Ref="J?"  Part="1" 
AR Path="/6915B170/691DDA03" Ref="J?"  Part="1" 
F 0 "J?" H 2678 3151 50  0000 L CNN
F 1 "Conn_01x02_Female" H 2678 3060 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-02A_1x02_P2.54mm_Vertical" H 2650 3175 50  0001 C CNN
F 3 "~" H 2650 3175 50  0001 C CNN
	1    2650 3175
	1    0    0    -1  
$EndComp
Text Label 2150 3175 2    50   ~ 0
WG_US_Rx_SW_Raw
Wire Wire Line
	2150 3175 2450 3175
Text Label 2150 3275 2    50   ~ 0
GND
Wire Wire Line
	2150 3275 2450 3275
Text Label 2150 2550 2    50   ~ 0
WG_US_Rx_N_Raw
Text GLabel 3200 1300 1    50   Input ~ 0
VCC_3V3
Wire Wire Line
	5300 1550 5300 1850
$Comp
L Device:R R?
U 1 1 691E5058
P 2675 1675
AR Path="/691E5058" Ref="R?"  Part="1" 
AR Path="/69125121/691E5058" Ref="R?"  Part="1" 
AR Path="/6915B170/691E5058" Ref="R?"  Part="1" 
F 0 "R?" V 2775 1625 50  0000 C CNN
F 1 "1k" V 2575 1650 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 2605 1675 50  0001 C CNN
F 3 "~" H 2675 1675 50  0001 C CNN
	1    2675 1675
	0    -1   1    0   
$EndComp
Wire Wire Line
	2400 1675 2525 1675
Wire Wire Line
	3200 1300 3200 1475
$Comp
L 2N2222A:2N2222A_SOT23 Q?
U 1 1 691E5065
P 3100 1675
AR Path="/691E5065" Ref="Q?"  Part="1" 
AR Path="/69125121/691E5065" Ref="Q?"  Part="1" 
AR Path="/6915B170/691E5065" Ref="Q?"  Part="1" 
F 0 "Q?" H 3299 1721 50  0000 L CNN
F 1 "2N2222A_SOT23" H 3299 1630 50  0000 L CNN
F 2 "TO92254P470H750-3" H 3100 1675 50  0001 L BNN
F 3 "" H 3100 1675 50  0001 L BNN
F 4 "N/A" H 3100 1675 50  0001 L BNN "PARTREV"
F 5 "7.5 mm" H 3100 1675 50  0001 L BNN "MAXIMUM_PACKAGE_HEIGHT"
F 6 "121774" H 3100 1675 50  0001 L BNN "SNAPEDA_PACKAGE_ID"
F 7 "IPC 7351B" H 3100 1675 50  0001 L BNN "STANDARD"
F 8 "Diotec Semiconductor" H 3100 1675 50  0001 L BNN "MANUFACTURER"
	1    3100 1675
	1    0    0    -1  
$EndComp
Wire Wire Line
	2825 1675 3000 1675
Wire Wire Line
	3200 1875 3200 2075
Wire Wire Line
	3200 2075 3325 2075
Text HLabel 2400 1675 0    50   Input ~ 0
WG_control
Wire Wire Line
	3675 3900 3800 3900
Wire Wire Line
	3800 3900 3800 4250
Text GLabel 3325 2075 2    50   Output ~ 0
WG_Power_3.3V
Text GLabel 5100 1550 0    50   Input ~ 0
WG_Power_3.3V
Wire Wire Line
	5100 1550 5300 1550
Text GLabel 3675 3900 0    50   Input ~ 0
WG_Power_3.3V
$Comp
L power:GND #PWR?
U 1 1 691FCFAA
P 6325 5200
AR Path="/691FCFAA" Ref="#PWR?"  Part="1" 
AR Path="/6915B170/691FCFAA" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6325 4950 50  0001 C CNN
F 1 "GND" H 6330 5027 50  0000 C CNN
F 2 "" H 6325 5200 50  0001 C CNN
F 3 "" H 6325 5200 50  0001 C CNN
	1    6325 5200
	1    0    0    -1  
$EndComp
$EndSCHEMATC
