EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L SamacSys_Parts:MAX232IDR IC?
U 1 1 69158AF6
P 5075 3675
AR Path="/69158AF6" Ref="IC?"  Part="1" 
AR Path="/69125121/69158AF6" Ref="IC?"  Part="1" 
F 0 "IC?" H 5675 3940 50  0000 C CNN
F 1 "MAX232IDR" H 5675 3849 50  0000 C CNN
F 2 "SOIC127P600X175-16N" H 6125 3775 50  0001 L CNN
F 3 "https://datasheet.datasheetarchive.com/originals/distributors/Datasheets-SFU2/DSASFU100031433.pdf" H 6125 3675 50  0001 L CNN
F 4 "MAX232IDR, Dual Line Transceiver, EIA/TIA-232-F, RS-232, V.28 2-TX 2-RX 2-TRX, 5 V, 16-Pin SOIC" H 6125 3575 50  0001 L CNN "Description"
F 5 "1.75" H 6125 3475 50  0001 L CNN "Height"
F 6 "595-MAX232IDR" H 6125 3375 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Texas-Instruments/MAX232IDR?qs=paYhMW8qfitI1Tq02IMHqw%3D%3D" H 6125 3275 50  0001 L CNN "Mouser Price/Stock"
F 8 "Texas Instruments" H 6125 3175 50  0001 L CNN "Manufacturer_Name"
F 9 "MAX232IDR" H 6125 3075 50  0001 L CNN "Manufacturer_Part_Number"
	1    5075 3675
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 69158AFC
P 3475 3825
AR Path="/69158AFC" Ref="C?"  Part="1" 
AR Path="/69125121/69158AFC" Ref="C?"  Part="1" 
F 0 "C?" V 3600 3825 50  0000 C CNN
F 1 "1uF" V 3675 3825 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 3513 3675 50  0001 C CNN
F 3 "~" H 3475 3825 50  0001 C CNN
	1    3475 3825
	-1   0    0    1   
$EndComp
Wire Wire Line
	3475 3975 3625 3975
Wire Wire Line
	3625 3975 3625 3875
$Comp
L Device:C C?
U 1 1 69158B04
P 3475 4275
AR Path="/69158B04" Ref="C?"  Part="1" 
AR Path="/69125121/69158B04" Ref="C?"  Part="1" 
F 0 "C?" V 3600 4275 50  0000 C CNN
F 1 "1uF" V 3675 4275 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 3513 4125 50  0001 C CNN
F 3 "~" H 3475 4275 50  0001 C CNN
	1    3475 4275
	-1   0    0    1   
$EndComp
Wire Wire Line
	3675 3975 3675 4125
Wire Wire Line
	3675 4125 3475 4125
Wire Wire Line
	3475 4425 3800 4425
Wire Wire Line
	3800 4425 3800 4075
$Comp
L Device:C C?
U 1 1 69158B0F
P 4875 2375
AR Path="/69158B0F" Ref="C?"  Part="1" 
AR Path="/69125121/69158B0F" Ref="C?"  Part="1" 
F 0 "C?" V 5000 2375 50  0000 C CNN
F 1 "1uF" V 5075 2375 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 4913 2225 50  0001 C CNN
F 3 "~" H 4875 2375 50  0001 C CNN
	1    4875 2375
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 69158B15
P 5200 2375
AR Path="/69158B15" Ref="C?"  Part="1" 
AR Path="/69125121/69158B15" Ref="C?"  Part="1" 
F 0 "C?" V 4950 2375 50  0000 C CNN
F 1 "0.1uF" V 5050 2375 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 5238 2225 50  0001 C CNN
F 3 "~" H 5200 2375 50  0001 C CNN
	1    5200 2375
	-1   0    0    1   
$EndComp
Wire Wire Line
	4875 2225 5200 2225
Connection ~ 4875 2225
$Comp
L power:GND #PWR?
U 1 1 69158B1D
P 5025 2600
AR Path="/69158B1D" Ref="#PWR?"  Part="1" 
AR Path="/69125121/69158B1D" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5025 2350 50  0001 C CNN
F 1 "GND" H 5030 2427 50  0000 C CNN
F 2 "" H 5025 2600 50  0001 C CNN
F 3 "" H 5025 2600 50  0001 C CNN
	1    5025 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4875 2525 4875 2600
Wire Wire Line
	4875 2600 5025 2600
Wire Wire Line
	5025 2600 5200 2600
Wire Wire Line
	5200 2600 5200 2525
Connection ~ 5025 2600
$Comp
L power:GND #PWR?
U 1 1 69158B28
P 7125 3775
AR Path="/69158B28" Ref="#PWR?"  Part="1" 
AR Path="/69125121/69158B28" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7125 3525 50  0001 C CNN
F 1 "GND" H 7130 3602 50  0000 C CNN
F 2 "" H 7125 3775 50  0001 C CNN
F 3 "" H 7125 3775 50  0001 C CNN
	1    7125 3775
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 69158B2E
P 3475 4725
AR Path="/69158B2E" Ref="C?"  Part="1" 
AR Path="/69125121/69158B2E" Ref="C?"  Part="1" 
F 0 "C?" V 3600 4725 50  0000 C CNN
F 1 "1uF" V 3675 4725 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 3513 4575 50  0001 C CNN
F 3 "~" H 3475 4725 50  0001 C CNN
	1    3475 4725
	-1   0    0    1   
$EndComp
Wire Wire Line
	3900 4175 3900 4575
$Comp
L power:GND #PWR?
U 1 1 69158B35
P 3475 4950
AR Path="/69158B35" Ref="#PWR?"  Part="1" 
AR Path="/69125121/69158B35" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3475 4700 50  0001 C CNN
F 1 "GND" H 3480 4777 50  0000 C CNN
F 2 "" H 3475 4950 50  0001 C CNN
F 3 "" H 3475 4950 50  0001 C CNN
	1    3475 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3475 4875 3475 4950
$Comp
L Device:C C?
U 1 1 69158B3C
P 4675 3350
AR Path="/69158B3C" Ref="C?"  Part="1" 
AR Path="/69125121/69158B3C" Ref="C?"  Part="1" 
F 0 "C?" V 4800 3350 50  0000 C CNN
F 1 "1uF" V 4875 3350 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 4713 3200 50  0001 C CNN
F 3 "~" H 4675 3350 50  0001 C CNN
	1    4675 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5075 3775 4675 3775
Wire Wire Line
	4675 3775 4675 3500
Text Label 6275 4175 0    50   ~ 0
US_Tx_SqW
Wire Wire Line
	6275 3775 7125 3775
Text Label 6275 4275 0    50   ~ 0
US_Tx_SqWN
Text Label 6275 3875 0    50   ~ 0
US_Tx_SqW_power
Text Label 5075 4275 2    50   ~ 0
US_Tx_SqWN_power
Wire Wire Line
	3475 3675 5075 3675
Wire Wire Line
	3625 3875 5075 3875
Wire Wire Line
	3675 3975 5075 3975
Wire Wire Line
	3800 4075 5075 4075
Wire Wire Line
	3475 4575 3900 4575
Wire Wire Line
	3900 4175 5075 4175
NoConn ~ 6275 3975
NoConn ~ 6275 4075
NoConn ~ 6275 4375
NoConn ~ 5075 4375
$Comp
L Device:R R?
U 1 1 69158B54
P 4650 1200
AR Path="/69158B54" Ref="R?"  Part="1" 
AR Path="/69125121/69158B54" Ref="R?"  Part="1" 
F 0 "R?" V 4750 1150 50  0000 C CNN
F 1 "1k" V 4550 1175 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 4580 1200 50  0001 C CNN
F 3 "~" H 4650 1200 50  0001 C CNN
	1    4650 1200
	0    -1   1    0   
$EndComp
Wire Wire Line
	4375 1200 4500 1200
Wire Wire Line
	5175 825  5175 1000
$Comp
L 2N2222A:2N2222A_SOT23 Q?
U 1 1 69158B61
P 5075 1200
AR Path="/69158B61" Ref="Q?"  Part="1" 
AR Path="/69125121/69158B61" Ref="Q?"  Part="1" 
F 0 "Q?" H 5274 1246 50  0000 L CNN
F 1 "2N2222A_SOT23" H 5274 1155 50  0000 L CNN
F 2 "TO92254P470H750-3" H 5075 1200 50  0001 L BNN
F 3 "" H 5075 1200 50  0001 L BNN
F 4 "N/A" H 5075 1200 50  0001 L BNN "PARTREV"
F 5 "7.5 mm" H 5075 1200 50  0001 L BNN "MAXIMUM_PACKAGE_HEIGHT"
F 6 "121774" H 5075 1200 50  0001 L BNN "SNAPEDA_PACKAGE_ID"
F 7 "IPC 7351B" H 5075 1200 50  0001 L BNN "STANDARD"
F 8 "Diotec Semiconductor" H 5075 1200 50  0001 L BNN "MANUFACTURER"
	1    5075 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 1200 4975 1200
Text Label 5300 1600 0    50   ~ 0
WG_US_Power_5V
Wire Wire Line
	5175 1400 5175 1600
Wire Wire Line
	5175 1600 5300 1600
Text Label 4875 2100 2    50   ~ 0
WG_US_Power_5V
Wire Wire Line
	4875 2100 4875 2225
Text Label 4525 3125 2    50   ~ 0
WG_US_Power_5V
Wire Wire Line
	4525 3125 4675 3125
Wire Wire Line
	4675 3125 4675 3200
Text GLabel 5000 825  0    50   Input ~ 0
5V
Wire Wire Line
	5000 825  5175 825 
Text HLabel 4375 1200 0    50   Input ~ 0
US_control
$Comp
L Connector:Conn_01x02_Female J?
U 1 1 692369AA
P 5425 6225
AR Path="/692369AA" Ref="J?"  Part="1" 
AR Path="/69125121/692369AA" Ref="J?"  Part="1" 
F 0 "J?" H 5453 6201 50  0000 L CNN
F 1 "Conn_01x02_Female" H 5453 6110 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-02A_1x02_P2.54mm_Vertical" H 5425 6225 50  0001 C CNN
F 3 "~" H 5425 6225 50  0001 C CNN
	1    5425 6225
	1    0    0    -1  
$EndComp
Wire Wire Line
	4925 6225 5225 6225
Wire Wire Line
	4925 6325 5225 6325
Text Label 4925 6325 2    50   ~ 0
US_Tx_SqWN_power
Text Label 4925 6225 2    50   ~ 0
US_Tx_SqW_power
Wire Wire Line
	6275 3125 6275 3675
Wire Wire Line
	4675 3125 6275 3125
Connection ~ 4675 3125
$EndSCHEMATC
